package training;

import java.util.List;

public class DVD extends Item {
    private MovieGenre genre;
    private String director;
    private List<String> cast;
    private String producer;


    public DVD(String title, String id, int year, MovieGenre genre, String director, List<String> cast, String producer) {
        super(title, id, year);
        this.genre = genre;
        this.director = director;
        this.cast = cast;
        this.producer = producer;
    }
    
}

enum MovieGenre {
    ACTION, HORROR, COMEDY, SCIENCE_FICTION, ROMANCE, ANIMATION, WESTERN, CRIME, DRAMA; 
}