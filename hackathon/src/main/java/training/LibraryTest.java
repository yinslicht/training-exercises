package training;

import java.util.ArrayList;
import java.util.Date;

public class LibraryTest{
    public static void main(String[] args) {
        //create an empty library
        Library lib = new Library();
        
        //create 3 book items
        Book harryPotter7 = new Book("Harry Potter and the Deathly Hallows",  "asldfkja8578957", 2008,"JK Rowling", 234762346,  BookGenre.FANTASY, "Scholastic");
        Book greatExpectations = new Book("Great Expectations", "jghuire84yhujyhwt", 1898, "Charles Dickens", 1569865654, BookGenre.MYSTERY, "self" );
        Book huckFinn = new Book("The Adventures of Huckleberry Finn", "asdfhuiesrf9aeh8f", 1908, "Mark Twain", 15456454, BookGenre.MYSTERY, "self");
    
        //create 3 CDs
        CD one = new CD("1", "asdlkfj8sef9", 1965, MusicGenre.ROCK, "The Beatles", "Universal");
        CD backInBlack = new CD("Back in Black", "jkjf8dfjkhfe", 1980, MusicGenre.ROCK, "ACDC", "Leidespleen Press B.V.");
        CD abbeyRoad = new CD("Abbey Road", "jfkdjsl99f8ds", 1969, MusicGenre.ROCK, "The Beatles", "Universal");
        
        //create 3 DVDs
        DVD shawshank = new DVD("The Shawshank Redemption", "asdjkf8rherk", 1994, MovieGenre.DRAMA, "Frank Darabont", new ArrayList<String>(){{add("Morgan Freeman"); add("Tim Robbins");}}, "Niki Marvin");
        DVD schindlers = new DVD("Schindlers List", "asdhvbvf8rherk", 1993, MovieGenre.DRAMA, "Steven Spielberg", new ArrayList<String>(){{add("Liam Neeson"); add("Ben Kingsley");}}, "Steven Spielberg");
        DVD sol = new DVD("The Silence of the Lambs", "asdjkffasjadshck", 1991, MovieGenre.HORROR, "Jonathan Demme", new ArrayList<String>(){{add("Jodie Foster"); add("Anthony Hopkins");}}, "Kenneth Utt");

        //create 3 periodicals
        Periodical nyt = new Periodical("New York Times", "sdfivnjrh3983", 2018, PeriodicalGenre.NEWSPAPER, 325, 43, new Date(2018, 7, 28));
        Periodical nyp = new Periodical("New York Post", "cbbghjsdfg875784", 2020, PeriodicalGenre.NEWSPAPER, 567, 3, new Date(2020, 4, 21));
        Periodical time = new Periodical("Time", "sdfasd4483", 2009, PeriodicalGenre.MAGAZINE, 300, 40, new Date(2009, 7, 1));

        //add all items to the library
        lib.addItem(harryPotter7);
        lib.addItem(greatExpectations);
        lib.addItem(huckFinn);
        lib.addItem(one);
        lib.addItem(backInBlack);
        lib.addItem(abbeyRoad);
        lib.addItem(shawshank);
        lib.addItem(schindlers);
        lib.addItem(sol);
        lib.addItem(nyt);
        lib.addItem(nyp);
        lib.addItem(time);

        //print all items in the library utilizing the toString method of Item
        for(Item i: lib.getItems().values()){
            System.out.println(i);
        }
        System.out.println();
        //remove two items from library
        lib.removeItem(one);
        lib.removeItem(nyt);

        //confirm that "one" and "nyt" are now removed from the library
        for(Item i: lib.getItems().values()){
            System.out.println(i);
        }
        
        System.out.println();

        //borrow a dvd
        lib.borrowItem("The Silence of the Lambs");
        
        //confirm that Silence of the Lambs is no longer available
        for(Item i: lib.getItems().values()){
            System.out.println(i);
        }

        System.out.println();

        //return Silence of the Lambs
        lib.returnItem("The Silence of the Lambs");

        //confirm that Silence of the Lambs is available again
        for(Item i: lib.getItems().values()){
            System.out.println(i);
        }
        System.out.println();

        //try borrowing a periodical
        lib.borrowItem("New York Post");

        //confirm that nothing changed
        for(Item i: lib.getItems().values()){
            System.out.println(i);
        }

    }
}