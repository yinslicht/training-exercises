package training;

import java.util.Hashtable;

public class Library {

    //Every item is inputed with its name as the key in a Hashtable to simplify access from the public trying to borrow items
    private Hashtable<String, Item> items; 

    public Library(){
        items = new Hashtable<>();
    }
    
    
    public void addItem(Item item){
        item.setAvailable(true);
        items.put(item.getTitle(), item);
        
    }

    public void removeItem(Item item){
        items.remove(item.getTitle());
    }
    //returns true if item was available, false if not 
    public boolean borrowItem(String title){
    
        //check that the item exists
        if(!items.containsKey(title)){
            System.out.println("requested item is not in library");
            return false;
        }
        Item item = items.get(title);
        
        //reject if periodical
        if(item instanceof Periodical){
            System.out.println("one may not borrow periodicals");
            return false;
        }
        if(item.isAvailable()){
            System.out.println("Borrow successful!");
            item.setAvailable(false);
            return true;
        }
        return false;

    }

    public void returnItem(String title){
        Item item = items.get(title);
        item.setAvailable(true);
    }

    public Hashtable<String, Item> getItems() {
        return items;
    }


}