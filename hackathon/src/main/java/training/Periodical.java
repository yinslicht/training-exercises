package training;

import java.util.Date;

public class Periodical extends Item {

    private PeriodicalGenre genre;
    private int volume;
    private int issue;
    private Date date;

    public Periodical(String title, String id, int year, PeriodicalGenre genre, int volume, int issue, Date date) {
        super(title, id, year);
        this.genre = genre;
        this.volume = volume;
        this.issue = issue;
        this.date = date;
    }
   
}

enum PeriodicalGenre{
    NEWSPAPER, MAGAZINE, DIGEST, REFERENCE, JOURNAL;
}