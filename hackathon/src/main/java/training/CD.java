package training;

public class CD extends Item {

    private MusicGenre genre;
    private String artist;
    private String record_label;

    public CD(String title, String id, int year, MusicGenre genre, String artist, String record_label) {
        super(title, id, year);
        this.genre = genre;
        this.artist = artist;
        this.record_label = record_label;
    }
    
}

enum MusicGenre {
    ROCK, HIP_HOP, POP, R_AND_B, FOLK, COUNTRY, JAZZ, HEAVY_METAL; 
}