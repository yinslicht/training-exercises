package training;

public class Book extends Item {
    private String author;
    private int isbn;
    private BookGenre genre;
    private String publisher;

    public Book(String title, String id, int year, String author, int isbn, BookGenre genre, String publisher) {
        super(title, id, year);
        this.author = author;
        this.isbn = isbn;
        this.genre = genre;
        this.publisher = publisher;
    }
}

enum BookGenre {
    FANTASY, SCIENCE_FICTION, MYSTERY, ROMANCE, NON_FICTION, POETRY; 
}