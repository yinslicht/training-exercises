package training;

public abstract class Item {
    private String title;
    private String id;
    private boolean available;
    private int yearPublished;

    // items start off not being available and are only marked as available once
    // they are added to the library
    public Item(String title, String id, int yearPublished) {
        this.title = title;
        this.id = id;
        this.available = false;
        this.yearPublished = yearPublished;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    public int getYearPublished() {
        return yearPublished;
    }

    public void setYearPublished(int yearPublished) {
        this.yearPublished = yearPublished;
    }

    @Override
    public String toString() {
        return title + ", " + yearPublished + ", " + (available?"available":"not available");
    }

    
}