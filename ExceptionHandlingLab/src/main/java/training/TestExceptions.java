public class TestExceptions {
    public static void main(String[] args) {
        try {
            Account[] accounts = { new SavingsAccount("Fingers", 2), new SavingsAccount("Jerry", 4),
                    new CurrentAccount("Mickey", 6) };
        } catch (DodgyNameException e) {
            System.out.println(e.toString());
            return;
        } 

    }
}