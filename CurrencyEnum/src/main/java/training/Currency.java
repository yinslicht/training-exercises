package training;

public enum Currency {
    GBP('£'), EUR('€'), USD('$');

    private char symbol;

    private Currency(char symbol) {
        this.symbol = symbol;
    }

    public char getSymbol() {
        return symbol;
    }

    
}