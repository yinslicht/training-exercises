import java.util.Iterator;
import java.util.TreeSet;

public class CollectionsTest {
   public static void main(String[] args) {
    TreeSet<Account> accounts;
    accounts = new TreeSet<Account>((acc1, acc2) ->  {
        if(acc1.getBalance()>acc2.getBalance())
        return 1;
        else if(acc1.getBalance()<acc2.getBalance())
        return -1;
        else return 0;
    });
    accounts.add( new SavingsAccount("Tom", 2));
    accounts.add(new CurrentAccount("Mickey", 6));
    accounts.add(new SavingsAccount("Jerry", 4));
    
    
    Iterator<Account> it = accounts.iterator();

    while(it.hasNext()){
        Account a = it.next();
        System.out.println(a.getName() + ": " + a.getBalance());
        a.addInterest();
        System.out.println("New Balance: " + a.getBalance());
    }

    System.out.println("Now with a for-each loop...");
    for(Account a : accounts){
        System.out.println(a.getName() + ": " + a.getBalance());
        a.addInterest();
        System.out.println("New Balance: " + a.getBalance());
    }
   } 
}