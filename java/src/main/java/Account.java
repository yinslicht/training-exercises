public abstract class Account implements Detailable {
    private double balance;
    private String name;
    private static double interestRate = .1;

    public Account(String name, double balance) {
        this.name = name;
        this.balance = balance;
    }

    public Account() {
        this("Yehuda", 50);
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public abstract void addInterest();

    public static double getInterestRate() {
        return interestRate;
    }

    public static void setInterestRate(double interestRate) {
        Account.interestRate = interestRate;
    }

    public boolean withdraw(double amount) {
        if (amount <= balance) {
            balance -= amount;
            return true;
        }
        return false;
    }

    public boolean withdraw() {
        return withdraw(20);
    }

    @Override
    public String getDetails() {
        return "Name: " + name + " Balance: " + balance;
    }
    
}