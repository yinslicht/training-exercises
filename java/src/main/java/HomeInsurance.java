public class HomeInsurance implements Detailable {
    private double premium;
    private double excess;
    private double amountInsured;

    public HomeInsurance(double premium, double excess, double amountInsured) {
        this.premium = premium;
        this.excess = excess;
        this.amountInsured = amountInsured;
    }

    @Override
    public String getDetails() {
        return  "Premium: " + premium + " Excess: " + excess;
    }

    
}