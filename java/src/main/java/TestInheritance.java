public class TestInheritance {
    public static void main(String[] args) {
        Account[] accounts = {new SavingsAccount("Tom", 2), new SavingsAccount("Jerry", 4), new CurrentAccount("Mickey", 6)};
        for(int i =0; i < 3; i++){
            accounts[i].addInterest();
            System.out.println(accounts[i].getName()+": "+ accounts[i].getBalance());
        }
    }
}